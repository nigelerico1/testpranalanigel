package com.bri.myapplication.slider

import IntroSliderAdapter
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.bri.myapplication.MainActivity
import com.bri.myapplication.R
import com.bri.myapplication.TourFragment
import com.bri.myapplication.databinding.ActivitySliderBinding

class SliderActivity : AppCompatActivity() {

    private val fragmentList = ArrayList<Fragment>()
    private lateinit var binding: ActivitySliderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySliderBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val adapter = IntroSliderAdapter(this)
        binding.vpIntroSlider.adapter = adapter

        fragmentList.addAll(listOf(
            TourFragment(), TourFragment(), TourFragment()
        ))
        adapter.setFragmentList(fragmentList)

        binding.indicatorLayout.setIndicatorCount(adapter.itemCount)
        binding.indicatorLayout.selectCurrentPosition(0)

        registerListeners()
    }

    private fun registerListeners() {
        binding.vpIntroSlider.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                binding.indicatorLayout.selectCurrentPosition(position)

                if (position < fragmentList.lastIndex) {
                    binding.tvSkip.visibility = View.VISIBLE
                } else {
                    binding. tvSkip.visibility = View.VISIBLE
                }
            }
        })

        binding.tvSkip.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

    }
}