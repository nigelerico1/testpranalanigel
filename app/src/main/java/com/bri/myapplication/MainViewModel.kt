
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlin.random.Random


class MainViewModel(context: Context) : ViewModel() {

    private val _primeNumber = MutableLiveData<Int>()
    val primeNumber: LiveData<Int> get() = _primeNumber

    private var compositeDisposable = CompositeDisposable()


    fun generatePrime() {
        val prime = generateRandomPrime()
        _primeNumber.value = prime
    }

    private fun generateRandomPrime(): Int {
        var number: Int
        do {
            number = Random.nextInt(100)
        } while (!isPrime(number))
        return number
    }

    private fun isPrime(number: Int): Boolean {
        if (number <= 1) {
            return false
        }
        for (i in 2 until number) {
            if (number % i == 0) {
                return false
            }
        }
        return true
    }

    fun dispose(){
        compositeDisposable.dispose()
    }
}