package com.bri.myapplication


import MainViewModel
import MainViewModelFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.bri.myapplication.databinding.ActivityMainBinding
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        viewModel = ViewModelProvider(this, MainViewModelFactory(this))[MainViewModel::class.java]


        binding.btnGenerate.setOnClickListener {
            viewModel.generatePrime()
        }

        viewModel.primeNumber.observe(this) {
            binding.editText.setText(it.toString())
        }
    }

}